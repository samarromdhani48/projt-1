FROM node:latest

RUN mkdir -p /usr/src/app 

WORKDIR /usr/src/app

COPY package*.json /usr/src/app

RUN npm install

COPY . /usr/src/app

EXPOSE 3000

CMD [ "npm", "start" ]

# Use the official Node.js image as the base image
# FROM node:latest
# WORKDIR /usr/src/app
# COPY package.json .
# RUN npm install
# COPY . ./
# EXPOSE 3000
# CMD [ "npm", "start" ] 

# Copy the entire project to the container
#COPY . .

# Build the Angular app for production
#RUN ng build --prod

# Use a smaller, production-ready image as the final image
#FROM nginx:alpine

# Copy the production-ready Angular app to the Nginx webserver's root directory
#COPY --from=build /app/dist/your-angular-app /usr/share/nginx/html

# Expose port 80
#EXPOSE 80

# Start Nginx
#CMD ["nginx", "-g", "daemon off;"]